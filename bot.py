# bot.py

import os
import random
import yaml
import discord
from discord.ext import commands
from dotenv import load_dotenv
from difflib import get_close_matches


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()

settings = yaml.full_load(open("config.yml"))

bot = commands.Bot(command_prefix=settings['prefix'])
locked_ansible = True

class Sync:
    def ansible():
        # change to message
        print("syncing")
        import scrape_urls
        global ansible_docs
        ansible_docs = yaml.full_load(open(settings['ansible_file']))

    def linux():
        import scrape_urls_manpages
        global linux_docs
        linux_docs = yaml.full_load(open(settings['linux_file']))

class HelpOthers(commands.Cog):
        """ test comment """
        @commands.command(name='ansible_doc', help='Responds with links for ansible documentation')
        async def ansible_doc(self, ctx, arg):
                embedVar = discord.Embed(title="Docs", description="ansible documentation realated to " + arg, color=0x00ff00 )
                pages = ansible_docs.keys()
                matches = (get_close_matches(arg, pages))
                for match in matches:
                        embedVar.add_field(name=match, value=ansible_docs[match]['url'] , inline=False)
                await ctx.send(embed=embedVar)






        @commands.command(name='man', help='Responds with links for linux documentation')
        async def man(self, ctx, arg):
                embedVar = discord.Embed(title="Docs", description="linux documentation realated to " + arg, color=0x00ff00 )
                pages = linux_docs.keys()
                matches=(get_close_matches(arg, pages))
                for match in matches:
                        embedVar.add_field(name=match, value=linux_docs[match]['url'] , inline=False)
                await ctx.send(embed=embedVar)



        @commands.command(name='lmgtfy', help='Responds with links for Let Me Google That For You')
        async def lmgtfy(self, ctx, *args):
                urlq = '+'.join(args)
                embedVar = discord.Embed(title="Google Fu", description="Let Me Google " + urlq + " For You", color=0x00ff00 )
                link = "https://lmgtfy.app/?q=" + urlq
                embedVar.add_field(name="LMGTFY", value=link , inline=False)
                await ctx.send(embed=embedVar)


        @commands.command(name='ddg', help='Responds with links for Duck Duck GO')
        async def ddg(self, ctx, *args):
                urlq = '+'.join(args)
                embedVar = discord.Embed(title="Duck Duck Go", description="Because Its Better Then Google \nLet Me Google " + urlq + " For You", color=0x00ff00 )
                link = "https://duckduckgo.com/?q=" + urlq
                embedVar.add_field(name="Duck Duck Go", value=link , inline=False)
                await ctx.send(embed=embedVar)



        @commands.command(name='archwiki', help='Responds with links for arch-wiki search')
        async def archwiki(self, ctx, *args):
                urlnice = ' '.join(args)
                urlq = '+'.join(args)
                embedVar = discord.Embed(title="RTFM", description="Let Me Search Fo" + urlnice + " In The Arch Wiki For you", color=0x00ff00 )
                link = "https://wiki.archlinux.org/index.php?search=" + urlq
                embedVar.add_field(name=urlnice, value=link , inline=False)
                await ctx.send(embed=embedVar)



class ModCommands(commands.Cog):
         @commands.command(name='sync', help='Responds with links for arch-wiki search')
         async def sync(self, ctx, *args):
             embedVar = discord.Embed(title="Mod-Command", description="Command to sync files", color=0x00ff00 )
             if settings['modrole'] in [i.name.lower() for i in ctx.author.roles]:
                 Sync.ansible()
                 Sync.linux()
                 embedVar.add_field(name='Sync', value="Synced files" , inline=False)
             else:
                 embedVar.add_field(name='Sync', value="You are not a mod" , inline=False)
             await ctx.send(embed=embedVar)

Sync.ansible()
Sync.linux()
bot.add_cog(HelpOthers())
bot.add_cog(ModCommands())

bot.run(TOKEN)
