import requests
import yaml
import re
from bs4 import BeautifulSoup

settings = yaml.full_load(open("config.yml"))

url = 'https://docs.ansible.com/ansible/latest/collections/'
reqs = requests.get(url)
soup = BeautifulSoup(reqs.text, 'html.parser')
w = open(settings['ansible_file'], "w")
w.write("")
w.close()


urls = []
print_flag = False
for link in soup.find_all('a'):
    parsed = link.get('href')
    parsed_dir = re.sub('index.html\#.*', '', parsed)
    if parsed.startswith('.'):
        print_flag = False
    if print_flag:
        url_inside = url + parsed
        reqs_inside = requests.get(url_inside)
        soup_inside = BeautifulSoup(reqs_inside.text, 'html.parser')
        for link_inside in soup_inside.find_all('a', {"class": "reference internal"}):
            parsed_inside = link_inside.get('href')
            print_flag_inside = True
            if parsed_inside.startswith('.'):
                print_flag_inside = False
            if print_flag_inside:
                if link_inside.text != u'¶':
                    f = open(settings['ansible_file'], "a")
                    if link_inside.span is not None:
                        f.write('"' + link_inside.span.contents[0] + '":\n')
                        f.write('    url: ' + url +
                                parsed_dir + parsed_inside + '\n')
                        w.close()
    if 'https://docs.ansible.com/' in parsed:
        print_flag = True
