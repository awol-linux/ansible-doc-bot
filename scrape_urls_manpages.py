import requests
import yaml
import re 
from bs4 import BeautifulSoup 

settings = yaml.full_load(open("config.yml"))

w = open(settings['linux_file'], "w")
w.write("")
w.close

list = ['1', '2', '3', '4', '5', '6', '7', '8', 'l', 'm',]
for catagory in list:
    dir = 'https://linux.die.net/man/' + catagory  + "/"
    url = dir + "index.html" 
    print(url)
    reqs = requests.get(url) 
    soup = BeautifulSoup(reqs.text, 'html.parser')
  
    for link in soup.find_all('a'):
        parsed = link.get('href')
        if parsed:
            test = parsed.startswith('http')
            test2 = parsed.startswith('/')
            if not test:
                if not test2:
                     f = open(settings['linux_file'], "a")
                     f.write("'" + link.string + "':\n")
                     f.write("     url: " + dir + parsed + "\n")
