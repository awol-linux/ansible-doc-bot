# Discord-ansible-doc
> A Discord bot to read ansible documentation

## Setup

1. Create a discord bot

  - see https://discordpy.readthedocs.io/en/latest/discord.html
2. Install bot server software

  - clone the repository `git clone https://gitlab.com/awol-linux/ansible-doc-bot.git`
  - Install requirments `pip install -r requirments.txt`
  - create a .env file and put `DISCORD_TOKEN={your-bot-token}` inside it
  - run `python3 ./scrape_urls.py && python3 ./scrape_urls_manpage.py` in order to create the module list (you may want to run this periodically in order to update the module list)
  - start the bot `python3 ./bot.py`

3. Alternitivly you can use Docker
```
docker run --env DISCORD_TOKEN={your-bot-token} awollinux/ansible-doc-bot:1.0 
```
4. Once the bot is added to your server, you can interact with it using the commands listed below.

## Usage

Commands for this bot follow this structure: `./<command>`.

| Command | Description
|---------|-------------|
| `./ansible_doc <search term>` | Will search through the module list and respond with a link. |
| `./lmgtfy <search term>` | Will respond with a Let Me Google That For You link. |
| `./doc help` | Displays usage instructions. |
| `./ddg <search term>` | Displays a link to Duck Duck Go |
| `./man <search term>` | Displays a link to Linux Docs |
| `./help` | Displays command usage | 
| `./sync` | Update the locale cache |

## Configuration

Settings are stored in config.yml the defaults are:

| Setting | Value | Description |
|---------|-------|--------------|
| modrole | "moderator" | The Role necceary to execute mod commands
| ansible_file | "urls.yml" | The file where ansible documentation links are stored |
| linux_file | "linux_man.yml" | The file where linux documentation links are stored |
