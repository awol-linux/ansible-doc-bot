FROM python:alpine

COPY . /opt/app
WORKDIR /opt/app
RUN apk add gcc alpine-sdk python3-dev && pip install -r requirements.txt
Run python3 scrape_urls_manpages.py && python3 scrape_urls.py
CMD python3 bot.py

